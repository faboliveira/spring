/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.DeclareParents;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 *
 * @author faboliveira
 */
//@Component
@Aspect
public class Aop {

    @Before("execution(* com.example.AopController.getEnv(..))")
    public void adviceEnv(JoinPoint jp) {
        System.out.println("Before " + jp);
    }

    @Before("execution(* com.example.AopController.getProcs(..))")
    public void adviceProcs(JoinPoint jp) {
        System.out.println("Before " + jp);
    }

    @DeclareParents(value = "com.example.Logic+", defaultImpl = PrintImpl.class)
    public static Print printer;
    
    private final Logger logger = LoggerFactory.getLogger(Aop.class);

    @Before("execution(* com.example.AopController.*(..))")
    public void logAop(JoinPoint jp) {
        logger.info(jp.getSignature() + " invoked.");
    }

    @AfterReturning(value = "execution(* com.example.AopController.*(..)))", returning = "retVal")
    public void logOrder(JoinPoint jp, ResponseEntity<Object> retVal) {
        logger.info(String.format("[HTTP Status: %s] %s invoked - %s", retVal.getStatusCode(), jp.getSignature(), (retVal.getBody()).toString()));
    }

    @AfterReturning(value = "execution(* com.example.GlobalExceptionHandlingControllerAdvice.*(..))", returning = "retVal")
    public void logControllerExceptions(JoinPoint jp, ResponseEntity<Object> retVal) {
        logger.error(String.format("Resource Exception: [HTTP Status %s] - %s", retVal.getStatusCode(), (ErrorResource) retVal.getBody()));
    }
}
