/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author faboliveira
 */
@Configuration
@ComponentScan
public class AopConfig {
    @Bean
    public Aop aop() {
        return new Aop();
    }

//    @Bean
//    public MyLogger myLogger() {
//        return new MyLogger();
//    }

    @Bean
    public Logic logic() {
        return new LogicImpl();
    }

    @Bean
    public CDPlayer cdPlayer() {
        return new CDPlayer();
    }

}
