/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * @author faboliveira
 */
@RestController
@RequestMapping("/aop")
public class AopController {

    @Autowired
    private Logic logic;

    @Autowired
    @Qualifier("desfado")
    private CompactDisc cd;

    @Autowired
    private CDPlayer cdPlayer;

    @Autowired
    private List<CompactDisc> cds;

    @RequestMapping(value = "/env", method = GET)
    public Map<String, String> getEnv() {
        return logic.getEnv();
    }

    @RequestMapping(value = "/procs", method = GET)
    public List<String> getProcs() {
        return logic.getProcs();
    }

    @RequestMapping(value = "/write", method = GET)
    public void write() {
        ((Print) logic).writeToConsole();
    }

    @RequestMapping(value = "/cd", method = GET)
    public void playCD() {
        cd.play();
    }

    @RequestMapping(value = "/cds", method = GET)
    public void playCDs() {
        for (CompactDisc cd : cds) {
            cd.play();
        }
    }

    @RequestMapping(value = "/cdplayer", method = GET)
    public void playCDPlayer() {
        cdPlayer.play();
    }
}
