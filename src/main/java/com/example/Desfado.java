package com.example;

import org.springframework.stereotype.Component;

/**
 * Created by faboliveira on 5/4/2017.
 */
@Component
public class Desfado implements CompactDisc {
    private String title = "Desfado";
    private String artist = "Ana Moura";

    public void play() {
        System.out.println("Playing " + title + " by " + artist);
    }
}
