package com.example;

public class ErrorResource {
	private String message;
    private String requestUrl;
    private int httpStatus;
    private String errorMessage;
    private String exceptionTime;

    public ErrorResource() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getExceptionTime() {
        return exceptionTime;
    }

    public void setExceptionTime(String exceptionTime) {
        this.exceptionTime = exceptionTime;
    }

    @Override
    public String toString() {
        return "ErrorResource{" +
                "message='" + message + '\'' +
                ", requestUrl='" + requestUrl + '\'' +
                ", httpStatus=" + httpStatus +
                ", errorMessage='" + errorMessage + '\'' +
                ", exceptionTime='" + exceptionTime + '\'' +
                '}';
    }
}
