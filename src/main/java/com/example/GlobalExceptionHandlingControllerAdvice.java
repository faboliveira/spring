package com.example;

import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by faboliveira on 5/5/2017.
 */
@ControllerAdvice
public class GlobalExceptionHandlingControllerAdvice {

    @ExceptionHandler(ResourceNotFoundException.class)
    ResponseEntity<Object> handleNotFoundException(HttpServletRequest request, ResourceNotFoundException e) {
        ErrorResource error = new ErrorResource();
        error.setMessage("Object with the following ID: '" + e.getLocalizedMessage() + "' was not found. Please find the details of the error below.");
        error.setHttpStatus(HttpStatus.NOT_FOUND.value());
        error.setRequestUrl(request.getRequestURL().toString());
        error.setExceptionTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        error.setErrorMessage(HttpStatus.NOT_FOUND.getReasonPhrase());

        return new ResponseEntity<Object>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    ResponseEntity<Object> handleHttpRequestMethodNotSupportedException(HttpServletRequest request, HttpRequestMethodNotSupportedException e) {
        ErrorResource error = new ErrorResource();
        error.setMessage("The following request is not supported: " + e.getMethod() + " with the following path: " + request.getRequestURI()
                + ". Please refer to documentation or use the one of the following methods for your request: " + e.getSupportedHttpMethods());
        error.setHttpStatus(HttpStatus.METHOD_NOT_ALLOWED.value());
        error.setRequestUrl(request.getRequestURL().toString());
        error.setExceptionTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        error.setErrorMessage(HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase());

        return new ResponseEntity<Object>(error, HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    ResponseEntity<Object> handleHttpMediaTypeNotSupportedException(HttpServletRequest request, HttpMediaTypeNotSupportedException e) {
        ErrorResource error = new ErrorResource();
        error.setMessage("The following media type is not supported: " + request.getContentType() + ". Please refer to the documentation to view example requests.");
        error.setHttpStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value());
        error.setRequestUrl(request.getRequestURL().toString());
        error.setExceptionTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        error.setErrorMessage(HttpStatus.UNSUPPORTED_MEDIA_TYPE.getReasonPhrase());

        return new ResponseEntity<Object>(error, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    ResponseEntity<Object> handleHttpMessageNotReadableException(HttpServletRequest request, HttpMessageNotReadableException e) {
        ErrorResource error = new ErrorResource();
        error.setMessage("The request you are sending has wrong structure. Please refer to the documentation for the payload examples.");
        error.setHttpStatus(HttpStatus.BAD_REQUEST.value());
        error.setRequestUrl(request.getRequestURL().toString());
        error.setExceptionTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        error.setErrorMessage(HttpStatus.BAD_REQUEST.getReasonPhrase());

        return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    ResponseEntity<Object> handleInternalServerError(HttpServletRequest request, Exception e) {
        ErrorResource error = new ErrorResource();
        error.setMessage("Your request was not successful, please try again.");
        error.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setRequestUrl(request.getRequestURL().toString());
        error.setExceptionTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        error.setErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());

        return new ResponseEntity<Object>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
