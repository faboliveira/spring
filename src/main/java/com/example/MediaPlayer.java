package com.example;

/**
 * Created by faboliveira on 5/4/2017.
 */
public interface MediaPlayer {
    void play();
}
