package com.example;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * Created by faboliveira on 5/4/2017.
 */
@Component
//@Primary
public class SgtPeppers implements CompactDisc {
    private String title = "Sgt. Pepper's Lonely Hearts Club Band";
    private String artist = "The Beatles";

    public void play() {
        System.out.println("Playing " + title + " by " + artist);
    }

}
